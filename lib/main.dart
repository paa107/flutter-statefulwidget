import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // Этот виджет является корнем приложения
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        /*
         * Это Тема вашего приложения.
         * 
         * Запуск приложения осуществляется командой "flutter run" или 
         * клавишей F5 (если используется VSCode)
         * 
         * Не закрывая приложение измените цвет на Colors.green, после
         * чего нажмите "r" или просто сохраните документ и убедитесь,
         * что "горячай перезагрузка" (hot reload) работает.
         * 
         * При это счётчик в приложении обнуляться не будет, а изменения
         * вступят в силу без перезагрузки.
         */
        primarySwatch: Colors.blue,
        /*
         * visualDensity активирует адаптивную плотность для определения
         * компактности различных компонентов пользовательского интерфейса.
         * 
         * Для настольных платформ элементы управления будут меньше и
         * ближе расположены друг к другу, чем на мобильных устройствах.
         */
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: MyHomePage(title: 'Flutter Demo Home Page'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  /*
   * Это Домашняя страница вашего приложения. Это "Stateful" виджет, что
   * означает, что он имеет объект "State", который содержит поля со
   * значениями влияющими на внешний вид виджета.
   * 
   * Этот класс представляет конфигурацию состояния. Он содержит значения
   * (в данном случае "title") предоставленные родителем (в данном случае
   * App widget) и использует метод State для сборки страницы.
   * 
   * Поля в подклассе виджета всегда обозначаются как "final".
   */
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      /*
       * Вызов setState говорит Flutter-у, что произошли изменения и
       * Flutter повторно запускает метод сборки отображая новые значения.
       * 
       * Если бы _counter был изменен без вызова setState(), то метод
       * сборки не был бы вызван и обновления не произошло.
       */
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    /*
     * Этот метод запускается каждый раз, когда вызывается setState, как
     * это делается в методе _incrementCounter()
     * 
     * Flutter оптимизирован для того чтобы быстро перезапускать методы
     * сборки, так что вы можете пеерстраивать что угодно, вместо того чтобы
     * менять экземпляры виджетов индивидуально.
     */
    return Scaffold(
      appBar: AppBar(
        /*
         * Здесь берется значение из объекта MyHomePage, созданного методом
         * App.build, и используется для установки в заголовок приложения.
         */
        title: Text(widget.title),
      ),
      body: Center(
        /*
         * Center - виджет, который берет один элемент и позиционирует его
         * по центру родительского элемента.
         */
        child: Column(
          /*
           * 
           */
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headline4,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
